import React from "react";
import { Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import styled from "styled-components/native";

const YLogoContainer = styled(View)`
  border-width: 1px;
  border-color: white;
  width: 20px;
  height: 20px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 4px;
`;

const YLogo = styled(Text)`
  color: white;
`;

const HeaderContainer = styled(View)`
  background-color: #ff6600;
  height: 40px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const Title = styled(Text)`
  font-weight: bold;
`;

const Header = ({ onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <HeaderContainer>
      <YLogoContainer>
        <YLogo>Y</YLogo>
      </YLogoContainer>
      <Title>Hacker News</Title>
    </HeaderContainer>
  </TouchableOpacity>
);

export default Header;
