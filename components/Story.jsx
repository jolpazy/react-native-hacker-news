import React from "react";
import styled from "styled-components/native";
import { View, Text, Linking } from "react-native";
import Comments from "./Comments";

const Container = styled(View)`
  background-color: rgb(227, 250, 252);
  padding: 10px;
  flex: 1;
`;

const Title = styled(Text)`
  color: #ff6600;
  font-size: 16px;
`;

const TitleContainer = styled(View)`
  margin-bottom: 8px;
`;

const Story = ({
  route: {
    params: { by, title, score, url, kids },
  },
}) => (
  <Container>
    <TitleContainer>
      <Title onPress={url ? () => Linking.openURL(url) : null}>{title}</Title>
      <Text>{`Score: ${score}, by: ${by}`}</Text>
    </TitleContainer>
    {kids && <Comments kids={kids} />}
  </Container>
);
export default Story;
