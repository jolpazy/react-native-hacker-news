import React from "react";
import { Text } from "react-native";
import styled from "styled-components/native";

const StyledText = styled(Text)`
  color: red;
`;

const ErrorText = () => <StyledText>Sorry, an error occured ):</StyledText>;

export default ErrorText;
