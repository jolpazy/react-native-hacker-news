import * as React from "react";
import { Text, View, Linking } from "react-native";
import styled from "styled-components/native";

const Comments = styled(Text)`
  color: #337baa;
  text-align: right;
  border-width: 1px;
  border-color: #337baa;
  padding: 4px 6px;
  border-radius: 4px;
  max-width: 50%;
`;

const Title = styled(Text)`
  color: #ff6600;
  font-size: 16px;
  margin-bottom: 8px;
`;

const Flex = styled(View)`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const StyledView = styled(View)`
  background-color: rgb(211, 251, 255);
  margin-bottom: 8px;
  padding: 10px;
`;

const Info = styled(Text)`
  max-width: 50%;
`;

const StoryListItem = ({
  story: { descendants, url, title, by, score, kids },
  navigation,
}) => {
  const navigateToStoryPage = () =>
    navigation.navigate("Story", {
      by,
      title,
      score,
      kids,
      url,
    });

  const navigateToUrl = () => Linking.openURL(url);

  return (
    <StyledView>
      <Title onPress={url ? navigateToUrl : navigateToStoryPage}>{title}</Title>
      <Flex>
        <Info style={{ maxWidth: "50%" }}>{`By ${by}, score: ${score}`}</Info>
        {!!descendants && (
          <Comments onPress={navigateToStoryPage}>{`View ${descendants} ${
            descendants > 1 ? "comments" : "comment"
          }`}</Comments>
        )}
      </Flex>
    </StyledView>
  );
};

export default StoryListItem;
