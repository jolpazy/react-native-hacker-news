import React, { useState, useEffect } from "react";
import { View, Button, FlatList } from "react-native";
import styled from "styled-components/native";
import StoryListItem from "./StoryListItem";
import ErrorText from "./ErrorText";
import Header from "./Header";
import Spinner from "./Spinner";

const offset = 20;

const Container = styled(View)`
  background-color: rgb(227, 250, 252);
  flex: 1;
`;

const TopStories = ({ navigation }) => {
  const [fetching, setFetching] = useState(true);
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(offset);
  const [errorOccured, setErrorOccured] = useState(false);

  const [topStories, setTopStores] = useState([]);

  const [stories, setStories] = useState([]);

  const fetchTopStories = () => {
    setFetching(true);
    fetch("https://hacker-news.firebaseio.com/v0/topstories.json")
      .then((data) => data.json())
      .then((json) => {
        if (json.error) {
          setErrorOccured(true);
          return;
        }
        setTopStores(json);
      })
      .catch((e) => {
        console.error(e);
        setErrorOccured(true);
      })
      .finally(() => {
        setFetching(false);
      });
  };

  const fetchStories = () => {
    const forNow = topStories.slice(start, end);
    setStart(start + offset);
    setEnd(end + offset);
    setFetching(true);

    Promise.all(
      forNow.map((item) =>
        fetch(
          `https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`
        ).then((data) => data.json())
      )
    )
      .then((data) => setStories([...stories, ...data]))
      .catch((e) => {
        setErrorOccured(true);
        console.error(e);
      })
      .finally(() => setFetching(false));
  };

  const resetStories = () => {
    setStories([]);
    setStart(0);
    setEnd(offset);
    fetchTopStories();
  };

  useEffect(() => {
    fetchTopStories();
  }, []);

  useEffect(() => {
    if (topStories.length > 0) {
      fetchStories();
    }
  }, [topStories]);

  return (
    <Container>
      <Header onPress={resetStories} />
      {errorOccured && <ErrorText />}
      <>
        {stories.length > 0 && (
          <FlatList
            data={stories}
            renderItem={({ item }) => (
              <StoryListItem story={item} navigation={navigation} />
            )}
            keyExtractor={(item) => item.id.toString()}
          />
        )}
      </>

      {fetching && <Spinner />}

      {stories.length < topStories.length && !fetching && (
        <Button
          onPress={fetchStories}
          title={`Load ${offset} more`}
          color="#337baa"
          accessibilityLabel="Load more stories"
        />
      )}
    </Container>
  );
};

export default TopStories;
