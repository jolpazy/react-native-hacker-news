import React, { useState, useEffect } from "react";
import { Text, View, FlatList, TouchableOpacity } from "react-native";
import styled from "styled-components/native";
import ErrorText from "./ErrorText";
import HTMLView from "react-native-htmlview";
import Spinner from "./Spinner";

const StyledComment = styled(View)`
  color: black;
  margin: 8px 0;
  border-left-width: 1px;
  border-color: lightgray;
  padding: 4px 0 4px 16px;
`;

const DeletedComment = styled(Text)`
  color: lightgray;
  margin: 8px 0;
  border-left-width: 1px;
  border-color: lightgray;
  padding: 4px 0 4px 16px;
  font-style: italic;
`;

const By = styled(Text)`
  color: rgb(132, 129, 129);
`;

const ShowMore = styled(Text)`
  margin-top: 4px;
  color: #337baa;
  border-color: #337baa;
  border-width: 1px;
  border-radius: 4px;
  padding: 4px 6px;
  text-align: center;
`;

const Comment = ({ comment: { kids, deleted, by, text } }) => {
  const [showComments, setShowComments] = useState(false);

  if (deleted) {
    return <DeletedComment>deleted comment</DeletedComment>;
  }

  const numberOfComments = kids?.length;

  return (
    <StyledComment>
      <By>{`By: ${by}`}</By>
      <HTMLView value={text} />
      <>
        {numberOfComments > 0 && (
          <TouchableOpacity
            onPress={() => setShowComments(!showComments)}
            style={{ flex: 1, flexDirection: "row" }}
          >
            <ShowMore>
              {showComments
                ? "Hide replies"
                : `Show ${numberOfComments} ${
                    numberOfComments > 1 ? "replies" : "reply"
                  }`}
            </ShowMore>
          </TouchableOpacity>
        )}
        {showComments && <Comments kids={kids} />}
      </>
    </StyledComment>
  );
};

const Comments = ({ kids }) => {
  const [comments, setComments] = useState([]);
  const [errorOccured, setErrorOccured] = useState(false);
  const [fetching, setFetching] = useState(true);

  useEffect(() => {
    let mounted = true;
    Promise.all(
      kids.map((item) =>
        fetch(
          `https://hacker-news.firebaseio.com/v0/item/${item}.json?print=pretty`
        ).then((data) => data.json())
      )
    )
      .then((data) => {
        if (mounted) {
          setComments(data);
        }
      })
      .catch((e) => {
        if (mounted) {
          setErrorOccured(true);
        }
        console.error(e);
      })
      .finally(() => {
        if (mounted) {
          setFetching(false);
        }
      });
    return () => (mounted = false);
  }, []);

  return (
    <>
      {errorOccured && <ErrorText />}
      {fetching && <Spinner />}
      {comments.length > 0 && (
        <FlatList
          data={comments}
          renderItem={({ item }) => <Comment comment={item} />}
          keyExtractor={(item) => item.id.toString()}
        />
      )}
    </>
  );
};

export default Comments;
