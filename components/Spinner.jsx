import React from "react";
import { ActivityIndicator } from "react-native";
import styled from "styled-components/native";

const StyledSpinner = styled(ActivityIndicator)`
  margin: 8px 0;
`;

const Spinner = () => <StyledSpinner color="#ff6600" />;

export default Spinner;
