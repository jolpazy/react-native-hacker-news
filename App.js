import "react-native-gesture-handler";
import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import TopStories from "./components/TopStories";
import Story from "./components/Story";

const Stack = createStackNavigator();

const App = () => (
  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen
        name="TopStories"
        component={TopStories}
        options={{ title: "Top Stories" }}
      />
      <Stack.Screen name="Story" component={Story} />
    </Stack.Navigator>
  </NavigationContainer>
);

export default App;
